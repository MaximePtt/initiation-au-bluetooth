package com.example.bluetoothapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class Bluetooth {

    public static BluetoothAdapter blueToothAdapter;
    public static MainActivity activity;
    static BluetoothSocket bTSocket;
    static OutputStream outputStream;
    static InputStream inputStream;
    static boolean listening;


    /**
     * Initialise la classe Bluetooth pour tout le programme
     * (Comme un constructeur)
     * @param activity
    */
    public static void init(MainActivity activity) {
        // Récupération de l'adapter bluetooth de l'appareil.
        //!\\ Les appareils non physiques n'en ont pas !
        blueToothAdapter = BluetoothAdapter.getDefaultAdapter();
        Bluetooth.activity = activity; // On récupère l'activité en paramètre
    }

    /**
     * @return liste de tous les BluetoothDevice appairés à l'appareil
     */
    public static Set<BluetoothDevice> getAllDevides() {
        if (blueToothAdapter != null) {
            if (blueToothAdapter.isEnabled()) {
                Set<BluetoothDevice> devicesFound = blueToothAdapter.getBondedDevices();
                if (devicesFound.size() > 0) {
                    return devicesFound;
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Aucun appareil n'est lié", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity.getApplicationContext(), "Bluetooth désactivé", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity.getApplicationContext(), "Erreur Adapteur", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    /**
     * Declenche un thread de connexion à l'appareil passé en paramètre
     * @param bTDevice
     */
    public static void connect(BluetoothDevice bTDevice) {
        new Thread() {
            public void run() {
                DeviceActivity.log("Connecting...");
                BluetoothSocket temp = null;
                try {
                    temp = bTDevice.createRfcommSocketToServiceRecord(bTDevice.getUuids()[0].getUuid());
                } catch (IOException e) {
                    DeviceActivity.log("Could not create RFCOMM socket:");
                    DeviceActivity.log(e.getMessage());
                }
                bTSocket = temp;

                try {
                    bTSocket.connect();
                    DeviceActivity.log("Connected !");
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            beginListenForData(); // On commence le listening
                        }
                    });
                } catch (IOException e) {
                    DeviceActivity.log("Could not connect:");
                    DeviceActivity.log(e.getMessage());
                    try {
                        bTSocket.close();
                    } catch (IOException close) {
                        DeviceActivity.log("Could not close connection:");
                        DeviceActivity.log(e.getMessage());
                    }
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DeviceActivity.setEnable(true, Bluetooth.bTSocket != null && Bluetooth.bTSocket.isConnected());
                    }
                });
            }
        }.start();
    }

    /**
     * Envoie un String à l'appareil auquel on est connecté
     * @param toSend
     */
    public static void send(String toSend) throws IOException {
        outputStream = bTSocket.getOutputStream();
        outputStream.write(toSend.getBytes());
        outputStream.flush();
        DeviceActivity.log("\"" + toSend + "\" was send successfully !");
    }

    /**
     * Déclenche un Thread écoutant l'appareil auquel on est connecté et réceptionannt toute réception de String dans les logs
     */
    public static void beginListenForData() {
        try {
            inputStream = bTSocket.getInputStream();
        } catch (IOException e) {
            DeviceActivity.log("Error:");
            DeviceActivity.log(e.getMessage());
        }
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        final int[] readBufferPosition = {0};
        byte[] readBuffer = new byte[1024];
        listening = true;
        new Thread(new Runnable() {
            public void run() {
                DeviceActivity.log("Start listening");
                while (!Thread.currentThread().isInterrupted() && listening) {
                    try {
                        int bytesAvailable = inputStream.available();
                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            inputStream.read(packetBytes);
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition[0]];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition[0] = 0;

                                    handler.post(new Runnable() {
                                        public void run() {
                                            DeviceActivity.log(data);
                                        }
                                    });
                                } else {
                                    readBuffer[readBufferPosition[0]++] = b;
                                }
                            }
                        }
                    } catch (IOException e) {
                        DeviceActivity.log("Error:");
                        DeviceActivity.log(e.getMessage());
                    }
                }
            }
        }).start();
    }


    /**
     * Stop le Thread d'écoute
     */
    public static void stopListenForData(){
        DeviceActivity.log("Listening stopped");
        listening = false;
    }
}
