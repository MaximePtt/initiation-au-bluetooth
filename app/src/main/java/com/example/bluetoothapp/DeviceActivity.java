package com.example.bluetoothapp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DeviceActivity extends AppCompatActivity {

    private static TextView logs;
    private static Switch bluetooth, listen;
    private static Button connect, send;
    private TextView nom, mac;
    private static EditText toSend;

    private BluetoothDevice device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        Intent myIntent = this.getIntent();
        device = Bluetooth.blueToothAdapter.getRemoteDevice(myIntent.getStringExtra("device")); // Récuperation du device à partir de l'addresse

        logs = findViewById(R.id.logs);
        bluetooth = findViewById(R.id.bluetooth);
        connect = findViewById(R.id.connect);
        nom = findViewById(R.id.nomDevice);
        mac = findViewById(R.id.adresseMACDevice);
        send = findViewById(R.id.send);
        toSend = findViewById(R.id.toSend);
        listen = findViewById(R.id.listen);

        logs.setMovementMethod(new ScrollingMovementMethod()); // Rendre la textview scrollable
        nom.setText(device.getName());
        mac.setText(device.getAddress());


        setEnable(Bluetooth.blueToothAdapter.isEnabled(), false);

        setListeners();


    }

    private void setListeners() {
        bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Bluetooth.blueToothAdapter.isEnabled()) {
                    Bluetooth.blueToothAdapter.enable(); // On active le bluetooth
                    setEnable(true, false); // On dégrise les cases correspondantesfcv  
                    log("Bluetooth activé");
                } else {
                    Bluetooth.blueToothAdapter.disable(); // On désactive le bluetooth
                    setEnable(false, false); // On grise les cases correspondantes
                    log("Bluetooth désactivé");
                }
            }
        });

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bluetooth.connect(device);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bluetooth.send(toSend.getText().toString());
                    toSend.setText("");
                } catch (IOException e) {
                    DeviceActivity.log("Could not send data:");
                    DeviceActivity.log(e.getMessage());
                }
            }
        });

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listen.isChecked())
                    Bluetooth.beginListenForData();
                else{
                    Bluetooth.stopListenForData();
                }
            }
        });
    }

    public static void setEnable(boolean bluetoothState, boolean connectionState) {
        bluetooth.setChecked(bluetoothState);
        connect.setEnabled(bluetoothState);
        toSend.setEnabled(connectionState);
        send.setEnabled(connectionState);
        listen.setChecked(connectionState);
        listen.setEnabled(connectionState);
    }

    public static void log(String line) {
        SimpleDateFormat heureFormat = new SimpleDateFormat("hh:mm:ss");
        String hour = heureFormat.format(new Date());
        logs.append("[" + hour + "] " + line + "\n");
        new Thread() { // Thread qui permet de descendre en bas des logs
            public void run() {
                while (logs.canScrollVertically(1)) {
                    logs.scrollBy(0, 10);
                }
            }
        }.start();
    }
}