package com.example.bluetoothapp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.Set;

public class MainActivity extends AppCompatActivity {

    Button scanButton;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scanButton = findViewById(R.id.scan);
        list = findViewById(R.id.list);

        Bluetooth.init(this);

        this.setListeners();
    }

    private void setListeners(){
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Set<BluetoothDevice> devicesList = Bluetooth.getAllDevides();
                if(devicesList != null) {
                    CustomListAdapter monAdapter = new CustomListAdapter(MainActivity.this, devicesList);
                    list.setAdapter(monAdapter);
                }
            }
        });
    }
}