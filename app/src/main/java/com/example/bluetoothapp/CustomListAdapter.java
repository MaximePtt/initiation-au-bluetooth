package com.example.bluetoothapp;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

public class CustomListAdapter extends BaseAdapter {
    private Context context; //context
    private ArrayList<BluetoothDevice> items; //data source of the list adapter

    //public constructor
    public CustomListAdapter(Context context, Set<BluetoothDevice> items) {
        this.context = context;
        this.items = new ArrayList();
        this.items.addAll(items);
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_devices, parent, false);
        }

        // get the TextView for item name and item description
        TextView nom = convertView.findViewById(R.id.nom);
        TextView adresseMAC = convertView.findViewById(R.id.adresseMAC);
        TextView type = convertView.findViewById(R.id.type);
        TextView bondState = convertView.findViewById(R.id.bondState);
        TextView connexionState = convertView.findViewById(R.id.connexionState);

        //sets the text for item name and item description from the current item object
        nom.setText(items.get(position).getName());
        adresseMAC.setText(items.get(position).getAddress());

        switch (items.get(position).getType()) {
            case BluetoothDevice.DEVICE_TYPE_UNKNOWN:
                type.setText("DEVICE_TYPE_UNKNOWN");
                break;
            case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                type.setText("DEVICE_TYPE_CLASSIC");
                break;
            case BluetoothDevice.DEVICE_TYPE_LE:
                type.setText("DEVICE_TYPE_LE");
                break;
            case BluetoothDevice.DEVICE_TYPE_DUAL:
                type.setText("DEVICE_TYPE_DUAL");
                break;
            default:
                type.setText("" + items.get(position).getType());
                break;
        }

        switch (items.get(position).getBondState()) {
            case BluetoothDevice.BOND_BONDED:
                bondState.setText("BOND_BONDED");
                break;
            case BluetoothDevice.BOND_NONE:
                bondState.setText("BOND_NONE");
                break;
            case BluetoothDevice.BOND_BONDING:
                bondState.setText("BOND_BONDING");
                break;
            default:
                bondState.setText("" + items.get(position).getBondState());
                break;
        }

        BluetoothManager bm = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);

        switch (bm.getConnectionState(items.get(position), BluetoothGatt.GATT)) {
            case BluetoothProfile.STATE_CONNECTED:
                connexionState.setText("STATE_CONNECTED");
                break;
            case BluetoothProfile.STATE_CONNECTING:
                connexionState.setText("STATE_CONNECTING");
                break;
            case BluetoothProfile.STATE_DISCONNECTED:
                connexionState.setText("STATE_DISCONNECTED");
                break;
            case BluetoothProfile.STATE_DISCONNECTING:
                connexionState.setText("STATE_DISCONNECTING");
                break;
            default:
                connexionState.setText("" + bm.getConnectionState(items.get(position), BluetoothGatt.GATT));
                break;
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(context, DeviceActivity.class);
                myIntent.putExtra("device", items.get(position).getAddress());
                context.startActivity(myIntent);
            }
        });

        // returns the view for the current row
        return convertView;
    }
}