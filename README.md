# Initiation au Bluetooth via une application android
## Présentation
##### Application android permettant de :
* Lister les appareils associés en Bluetooth
* Voir les informations de chaque appareil associé (Nom, Adresse MAC, Type, Etat de l'appairage, Etat de la connexion)
* Se connecter à un des appareils appairés
* Envoyer un ___String___ à l'appareil en question
* Recevoir des ___String___ de l'appareil en question (si application installée sur les 2 appareils)
* Activer / Désactiver le Bluetooth

##### Auteurs :

* **Maxime Potet** (Developpement)
* **Tom-Olivier Colliaux** (Recherche sur les documentations)

##### Objectif
Le but de cette application est principalement d'acquérir une compréhension du système Bluetooth.<br/>
En plus de cet objectif, cette application nous a permis d'être plus à l'aide avec Android Studio

## Installation

Récupérer le fichier `/app.apk` sur un appareil android, puis l'installer.<br>
Après ceci, l'application est installée, il n'y a plus qu'à la lancer.

## Screenshots
![Accueil](screenshots/accueil.jpg)
![Accueil](screenshots/accueilScan.jpg)<br/>
![Accueil](screenshots/device.jpg)
![Accueil](screenshots/connecting1.jpg)<br/>
![Accueil](screenshots/connecting2.jpg)
